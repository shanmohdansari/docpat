from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Doctor, User, Appointment


class UserSignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2','gender')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'validate'
            })


class DoctorSignupForm(forms.ModelForm):
    class Meta:
        model = Doctor
        fields = ('education', 'department')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'validate'
            })
"""

class Book_appointmentForm(forms.ModelForm):
    class meta:
        model = Appointment
        fields = ('patient_address', 'disease', 'patient', 'symptoms', 'doctor')
"""